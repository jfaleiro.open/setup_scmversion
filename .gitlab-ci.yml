##
##     setup_scmversion - Automatic setting of semantic version numbers based
##                        on scm tags and branches.
##
##     Copyright (C) 2019 Jorge M. Faleiro Jr.
##
##     This program is free software: you can redistribute it and/or modify
##     it under the terms of the GNU Affero General Public License as published
##     by the Free Software Foundation, either version 3 of the License, or
##     (at your option) any later version.
##
##     This program is distributed in the hope that it will be useful,
##     but WITHOUT ANY WARRANTY; without even the implied warranty of
##     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##     GNU Affero General Public License for more details.
##
##     You should have received a copy of the GNU Affero General Public License
##     along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

#
# test with:
# gitlab-runner exec docker $1 --docker-volumes /var/run/docker.sock:/var/run/docker.sock
#     where $1 is one of the jobs declared in here
#
# alternativelly, use the helper job gitlab_ci_run_test.sh under
#     https://gitllab.com/jfaleiro/configuration project
#     see instructions on how to install
#
# for CI, this build requires a docker-in-docker by bind-mount of docker.sock, meaning gitlab-runner has to be
# pre-configured (gitlab-runner install) with specific parameters. For details:
#   https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding
#
#

image: python:3.8

variables:
   # speedup from git fetch (default of 50 is too low for most projects, 0 fetches all)
   GIT_STRATEGY: clone
   GIT_DEPTH: "0"
   # speedup across jobs
   # https://docs.gitlab.com/ee/ci/caching/#caching-python-dependencies
   PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
   key: ${CI_COMMIT_REF_SLUG}
   paths:
      - .cache/pip
      # - .tox
      # venv cannot be cached https://github.com/python-poetry/poetry/issues/1004
      # - .venv/

before_script:
   - echo "${CI_COMMIT_REF_SLUG}"
   # gitlab-ci BUG - gitlab uses a detached head on runners
   # https://gitlab.com/gitlab-org/gitlab-ce/issues/19421
   - git checkout $CI_COMMIT_REF_NAME
   # gitlab-ci having difficulties with git
   # on master causes "You are not currently on a branch. Please specify which branch you want to merge with"
   - echo "$CI_COMMIT_REF_NAME"
   - git rev-list --count $CI_COMMIT_REF_NAME
   - apt-get update
   - apt-get -y install gettext-base
   - pip install --upgrade pip
   - pip install poetry
   # - bin/tag-version.sh
   - poetry install --no-dev
   - poetry run scmversion version
   - VERSION_TYPE=$(poetry run scmversion version-type)
   - echo "VERSION_TYPE is $VERSION_TYPE"

check_version:
   stage: build
   script:
      - echo "if failure make sure pre-commit is in use on your local environment and versions are manually edited before tagging"
      - if [ "$VERSION_TYPE" = "RELEASE" ] || [ "$VERSION_TYPE" = "RELEASE_BRANCH" ] ; then
      - poetry run scmversion check
      - fi

build:
   stage: build
   script:
      - poetry install --no-dev
      - poetry build

.test:
   stage: test
   script:
      - poetry run make test

.test-3.6:
   extends: .test
   image: python:3.6

test-3.7:
   extends: .test
   image: python:3.7

test-3.8:
   extends: .test
   image: python:3.8

coverage:
   stage: test
   script:
      - poetry run make coverage
   coverage: "/TOTAL.+ ([0-9]{1,3}%)/"
   artifacts:
      paths:
         - build/coverage/html
      expire_in: 1 day

coverage.pages:
   stage: deploy
   dependencies:
      - coverage
   script:
      - mv build/coverage/html/ public/
   artifacts:
      paths:
         - public
      expire_in: 30 days

deploy:
   stage: deploy
   script:
      - poetry build --format wheel
      - if [ "$VERSION_TYPE" = "RELEASE" ] ; then
      - echo "deploying production using POETRY_PYPI_TOKEN_PYPI"
      # - poetry bug see https://github.com/python-poetry/poetry/issues/2801
      - poetry publish -u __token__ -p "$POETRY_PYPI_TOKEN_PYPI"
      - elif [ "$VERSION_TYPE" = "RELEASE_BRANCH" ]; then
      - echo "deploying production using POETRY_PYPI_TOKEN_TESTPYPI"
      # - poetry bug see https://github.com/python-poetry/poetry/issues/2801
      - poetry publish -u __token__ -p "$POETRY_PYPI_TOKEN_TESTPYPI" --repository testpypi -n
      - else
      - echo "VERSION_TYPE is $VERSION_TYPE - no deployment"
      - fi
