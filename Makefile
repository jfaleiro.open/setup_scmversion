#
#     setup_scmversion - Automatic setting of semantic version numbers based
#                        on scm tags and branches.
#
#     Copyright (C) 2019 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
.PHONY: test coverage deploy.staging deploy.production deploy headers
test:
	poetry install -E tests --no-dev
	python -m pytest --durations=0 -vv
	behave
headers:
	./setup.py adjust_license_headers
coverage:
	poetry install -E coverage --no-dev
	coverage run --omit='tests/*' --source setup_scmversion -m pytest
	coverage report -m
	coverage html -d build/coverage/html
prepare:
	# dummy version so we have something to install the entrypoint 'scmversion' from this project
	export POETRY_VERSION="0.0.0+noversion.0" && envsubst < pyproject.template.toml >| pyproject.toml
	# need --root-only option https://github.com/python-poetry/poetry/pull/3648
	poetry install --no-dev
	# bin/tag-version.sh
	scmversion tag-file --version POETRY_VERSION --pre-commit
	poetry install -E coverage -E tests -E interactive-dev
clean:
	rm -rf .tox build .pytest_cache
version:
	scmversion tag-version
