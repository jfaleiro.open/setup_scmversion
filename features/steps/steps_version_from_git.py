#
#     setup_utility - Shared helpers for setuptools configuration.
#
#     Copyright (C) 2019 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from behave import given, then
from hamcrest import assert_that, equal_to

from setup_scmversion.scm.git import GitParser


@given(u'the following tags and number of commits on master')
def step_impl(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='master', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]


@then(u'we will get a version according to tags and commits')
def step_then_will_get_version(context):
    for expected, actual in zip(
            context.expected_version,
            context.actual_version):
        assert_that(actual, equal_to(expected))


@then(u'we will get a version type according to tags and commits')
def step_impl2(context):
    for expected, actual in zip(
            context.expected_version_type,
            context.actual_version_type):
        assert_that(actual, equal_to(expected))


@given(u'the following tags and number of commits on master (detached HEAD)')
def step_impl3(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='HEAD', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]


@given(u'the following tags and number of commits on branch release/1.0.0')
def step_impl4(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='release/1.0.0', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]


@given(u'the following tags and number of commits on branch release/pineapple')
def step_impl5(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='release/pineapple', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]


@given(u'the following tags and number of commits on branch feature/pineapple')
def step_impl6(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='feature/pineapple', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]


@given(u'the following tags and number of commits on branch apple')
def step_impl7(context):
    context.expected_version = [row['version'] for row in context.table]
    context.expected_version_type = [row['version_type']
                                     for row in context.table]
    actuals = [GitParser.build_version(
        branch='apple', commits=row['commits'],
        tag=row['tag']) for row in context.table]
    context.actual_version_type = [actual[0].name for actual in actuals]
    context.actual_version = [actual[1] for actual in actuals]
