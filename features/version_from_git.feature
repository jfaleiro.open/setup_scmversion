Feature: Produce a pypi version based on git data
	A standard git repository carries mandatory attributes (branch
	and number of commits) and optional (latest tag).
	Based on branch, number of commits, and latest tag produce a
	pypi compatible semantic version number.


	Scenario: A code base on master
		Given the following tags and number of commits on master
			| tag   | commits | version                | version_type |
			|       | 1       | 0.0.0+master.nothing.1 | OTHER        |
			| 1.0.0 | 2       | 1.0.0       		   | RELEASE      |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits

	Scenario: A code base on master (detached HEAD)
		Given the following tags and number of commits on master (detached HEAD)
			| tag   | commits | version     		   | version_type |
			|       | 1       | 0.0.0+master.nothing.1 | OTHER        |
			| 1.0.0 | 2       | 1.0.0      			   | RELEASE      |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits

	Scenario: A code base on a release branch
		Given the following tags and number of commits on branch release/1.0.0
			| tag   | commits | version    | version_type   |
			|       | 1       | 1.0.0-dev1 | RELEASE_BRANCH |
			| 1.0.0 | 2       | 1.0.0-dev2 | RELEASE_BRANCH |
			|       | 3       | 1.0.0-dev3 | RELEASE_BRANCH |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits

		Given the following tags and number of commits on branch release/pineapple
			| tag   | commits | version                      | version_type   |
			|       | 1       | 0.0.0+release.invalid.1 | OTHER |
			| 1.0.0 | 2       | 0.0.0+release.invalid.2 | OTHER |
			|       | 3       | 0.0.0+release.invalid.3 | OTHER |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits

		Given the following tags and number of commits on branch feature/pineapple
			| tag   | commits | version                          | version_type   |
			|       | 1       | 0.0.0+feature.invalid.1 | OTHER |
			| 1.0.0 | 2       | 0.0.0+feature.invalid.2 | OTHER |
			|       | 3       | 0.0.0+feature.invalid.3 | OTHER |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits

		Given the following tags and number of commits on branch apple
			| tag   | commits | version          		  | version_type |
			|       | 1       | 0.0.0+other.apple.1 	  | OTHER        |
			| 1.0.0 | 2       | 0.0.0+other.apple.1.0.0.2 | OTHER        |
			|       | 3       | 0.0.0+other.apple.3 	  | OTHER        |

		Then we will get a version according to tags and commits
		And we will get a version type according to tags and commits
