from setup_scmversion.scm.git import is_valid_version


def test_malformed_semantic_version():
    assert not is_valid_version('0.0.0.feature12')
    assert not is_valid_version('0.0.1-1-g5c0bb91')
    assert not is_valid_version('0.0.4-2-ge87db8f')
    assert not is_valid_version('0.0.1-prev')


def test_welformed_semantic_version():
    assert is_valid_version('0.0+master.dev12')
    assert is_valid_version('0+master.dev12')
    assert is_valid_version('0.0.1')
    assert is_valid_version('0.0.1-dev12')
    assert is_valid_version('0.0.1-pre12')
    assert is_valid_version('0.0.1-post12')
    assert is_valid_version('0.0.0+master.dev12')
    assert is_valid_version('0.0.0+feature12')
